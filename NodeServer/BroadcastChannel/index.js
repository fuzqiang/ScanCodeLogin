const { BroadcastChannel } = require('broadcast-channel');

module.exports = {
  SubscribeExpired: new BroadcastChannel('SubscribeExpired'),
  SweepStatus: new BroadcastChannel('SweepStatus'),
  AuthorizedWeb: new BroadcastChannel('AuthorizedWeb')
};